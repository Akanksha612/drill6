function f1(arr)
{

   var result= arr.filter((obj) =>  (obj["job"].includes("Web Developer")) );
   return result;
}

module.exports.f1=f1;


//---------------------------------------------------------------------------------


function f2(arr)
{
    arr.map( (obj) => obj.salary= Number((obj.salary).substring(1)))

    return arr;
}

module.exports.f2=f2;




//--------------------------------------------------------------------------------------


function f3(arr)
{

    arr.map( (obj) => 
    {
        obj.corrected_salary= obj.salary *10000;
    });

    return arr;
} 

module.exports.f3=f3;  


//------------------------------------------------------------------------------------


function f4(arr)
{
    var result= arr.reduce( (accumulator, currentObj) =>
    {
        return accumulator+ currentObj.corrected_salary;

    },0);

    return result;
}

module.exports.f4=f4;

//----------------------------------------------------------------------------------------


function f5 (arr)
{
    var result = arr.reduce((accum,current) =>
    {

        if(accum.hasOwnProperty(current.location))
        {
            accum.location += current.salary;
        }

        else
        accum[current.location] = current.salary;

        return accum;

    }
         ,{});

         return result;


}


module.exports.f5=f5;

 

//----------------------------------------------------------------

function f6(arr)
{

    

    var final_result= arr.reduce((result,current) => 
    {

        if(result.hasOwnProperty(current.location))
        {
            result[current.location].count+=1;
            result[current.location].salary+=current.salary;
            result[current.location].average= result[current.location].salary/result[current.location].count;

        }

        else
        {
            result[current.location]= {"count": 1, "salary":current.salary, "average": current.salary};

        }
        return result;

    }, {});


return final_result;

}

module.exports.f6=f6;

//-------------------------------------------------------------------------------------------------------------